[Discuz! 的编码规范](?ac=document&page=dev_coderule) - 开发一个优秀的产品，需要有追求完美、严谨、高效的态度。在开始了解 Discuz! 的技术之前，我们有必要先阅读一下 Discuz! 的编码规范。本编码规范，仅适用于对 Discuz! 研发。

[Discuz! 的模板机制](?ac=document&page=dev_template) - 在编写Discuz!可用的模板之前，您应当对模板的创建流程有一个大致了解。你可以参考下面的流程进行创建。下面的文档，通过对模板的解析流程，缓存生成，以及模板语法和CSS缓存等多角度进行剖析，并通过简单示例的方式揭示整个流程的来龙去脉。

[Discuz! 的插件机制](?ac=document&page=dev_plugin) - Discuz! 拥有一套完整的插件机制。合理使用插件机制，你可以更好的将自己的个性功能需求整合到Discuz!产品中的同时，保持代码独立，并且不受后续Discuz!产品版本升级的影响。我们鼓励并推荐，使用插件机制进行个性需求定制和研发。

[Discuz开发文档资料](https://www.dismall.com/forum-49-1.html) - discuz应用中心社区大家分享的教程

[Discuz! 资料库](https://addon.dismall.com/library/) - Discuz! 功能使用、系统运维、二次开发、架构扩展。

~~[Discuz基础视频教程](https://pan.baidu.com/s/1dW-dse39Ovkpwvqw7Js1Pg)（提取码: 9kmg ） - Discuz!X零基础课程，本教程对于Discuz!安装、使用以及二次开发进行了详细讲解。~~

~~[cr180整理的Discuz!开发手册](http://discuzt.cr180.com/) - 该文档由cr180整理，以协助各位开发者或站长更好的了解Discuz!，更好的使用Discuz!，更好的创作自己的作品。~~

~~[Discuz! 的微信接口（已不推荐使用）](?ac=document&page=plugin_wechat) - Discuz! 新增的一套接入微信的插件接口，方便让开发者快速开发出微信类插件。~~

~~[Discuz! 的打通版微社区接口（已不推荐使用）](?ac=document&page=plugin_wsq)- Discuz! 新增的一套接入打通版微社区的插件接口，使您的应用通过微社区在微信中成为推广、营销、增强互动等各方面利器。~~


## Discuz! X3 范例插件
Discuz! X3 添加了诸多开放接口，此范例展示了增强的第三方拓展类和计划任务、缓存模块的设计方法

[本地下载范例插件](https://addon.dismall.com/resource/Discuz_X3.0_Plugin_Sample.zip)

[应用中心在线下载范例插件](https://addon.dismall.com/plugins/test.html)


## Discuz!插件开发助手
插件开发助手是面向Discuz！开发者，通过本扩展您可以快速生成您的插件原始代码。将最后导出的插件包导出到插件目录下，就可以开始您的插件开发之旅。哪怕您是一个新手开发者，也能快速从这个插件包中学习如何开发Discuz!插件

### 安装方法

1. 将本压缩包解压到Discuz!X根目录就可以了

2. 访问方式是`http://你的域名/develop.php`


[下载插件开发助手（GBK & UTF8）](https://addon.dismall.com/resource/develop_tool.zip) (请勿在线上论坛使用，仅限开发者本地开发环境使用)

## Discuz!DeBug模式
[DeBug模式解析文档](?ac=document&page=debug)

## Discuz!目录索引（基于X3.5）
[Discuz!目录索引文档](?ac=document&page=dir_index)