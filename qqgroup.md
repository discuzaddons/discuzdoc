<div class="tbmu bbda mbm"><a href="?ac=document&page=faq">开发者帐号</a><span class="pipe">|</span><a href="?ac=document&page=faq_addon">发布应用</a><span class="pipe">|</span><a href="?ac=document&page=faq_zip">文件包说明</a><span class="pipe">|</span><a href="?ac=document&page=faq_certification">身份认证</a><span class="pipe">|</span><a href="?ac=document&page=faq_api">开放接口</a><span class="pipe">|</span><a href="?ac=document&page=qqgroup" class="a">官方QQ群</a></div>

## 1、Discuz! 开发者群：694996266
[![](https://pub.idqqimg.com/wpa/images/group.png)](https://qm.qq.com/cgi-bin/qm/qr?k=cCtATS4nCyJThgIYst_ha1eRipiIBHJ7&jump_from=webapi, 'Discuz! 开发者新人群')

请凭开放平台 ID 申请入群，您可以在“[基本信息](?ac=edit&anchor=basesetting)”页面找到此 ID。

## 2、QQ 帐号与平台登记的一致
请凭此平台 ID 在基本信息中登记的 QQ 帐号提交入群申请。