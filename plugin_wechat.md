[TOCM]

## 接口概述
Discuz! 的微信接口由系统插件 “[掌上论坛](https://addon.dismall.com/plugins/mobile.html)”及“[微信登录](https://addon.dismall.com/plugins/wechat.html)” 版本提供，此接口我们通过插件形式向 Discuz! X2.5、Discuz! X3.x 版本提供，Discuz! 新版将内置此接口。此接口统一了 Discuz! 的微信接口地址，并且可以很方便让开发者开发出微信类插件，使开发者无需详细阅读微信公众平台的开发手册即可很方便的开发出微信相关的插件。

这 2 个插件已允许被其他插件合并安装，开发者可在插件的版本设置中设置合并安装“mobile.plugin.25962”和“wechat.plugin.35632”即可。

调用本接口前您需要在插件中引用接口文件
```
require_once DISCUZ_ROOT . './source/plugin/wechat/wechat.lib.class.php';
```

## 嵌入点接口
嵌入点接口用于把您开发的插件方法或数据注册到微信接口中

### 绑定微信 OpenID
```
WeChatHook::bindOpenId($uid, $openid, $isregister = 0)
```
此方法用于将一个用户的微信 OpenID 与论坛帐号绑定，方便用户使用微信登录

|参数|参数含义|
|-----:|-----|
|`$uid`|用户 Id|
|`$openid`|微信 OpenID|
|`$isregister`|是否为新注册用户|


### 注册微信响应嵌入点
```
WeChatHook::updateResponse($data, $extId = '')
```
此方法可以将一个方法注册到微信的事件嵌入点上

|参数|参数含义|
|-----:|-----|
|`$data`|详见下面的《响应嵌入点》|
|`$extId`|扩展 ID，留空表示更新默认嵌入点<br />调用扩展的嵌入点时接口地址是`http://yourwebsite/api/mobile/?module=wechat&id=extId`|

### 获取微信响应嵌入点列表
```
WeChatHook::getResponse($extId = '')
```
此方法可以获取当前注册的所有的响应嵌入点信息

|参数|参数含义|
|-----:|-----|
|`$extId`|扩展 ID，字母及数字的组合，留空表示获取默认嵌入点|

### 注册扩展开发者凭据信息
```
updateAppInfo($extId, $appId = '', $appSecret = '')
```
此方法可以注册一个开发者凭据信息

|参数|参数含义|
|-----:|-----|
|`$extId`|扩展 ID|
|`$appId`|待注册的 appId，留空表示删除此扩展 ID|
|`$appSecret`|待注册的 appSecret，留空表示删除此扩展 ID|

### 获取扩展开发者凭据信息
```
WeChatHook::getAppInfo($extId)
```
此方法可以获取一个已注册的开发者凭据信息

|参数|参数含义|
|-----:|-----|
|`$extId`|扩展 ID|

> WeChatClient 实例化时可直接携带此 $extId

```
$WeChatClient = new WeChatClient($extId)
```

### 注册微信跳转 URL
```
WeChatHook::updateRedirect($data)
```
此方法可以注册一个微信跳转 URL 方法，此方法将在微信用户登录、注册成功后触发

|参数|参数含义|
|-----:|-----|
|`$data`|详见下面的《跳转嵌入点》|

### 获取跳转 URL
```
WeChatHook::getRedirect()
```
此方法可以获取当前注册的获取跳转 URL 嵌入点信息

### 注册微信前端插件标识
```
WeChatHook::updateViewPluginId($value)
```
此方法可以把一个插件的 PC 前端功能注册到微信登录中

|参数|参数含义|
|-----:|-----|
|`$value`|插件标识|

>此插件需要准备“lang_wechat_logintip、lang_wechat_login、lang_wechat_bind、lang_wechat_threadmessage”4个语言包项目以供显示

### 获取微信前端插件标识
```
WeChatHook::getViewPluginId()
```
此方法可以获取当前注册的前端功能插件的标识


## 响应嵌入点
### 响应嵌入点格式
```
array(
  嵌入点 => 注册参数
)
```
### 嵌入点包含以下内容

|嵌入点|含义|
|-----:|-----|
|`receiveAllStart`|全局开始|
|`receiveMsg::text`|文本消息|
|`receiveMsg::location`|地理位置消息|
|`receiveMsg::image`|图片消息|
|`receiveMsg::video`|视频消息|
|`receiveMsg::link`|链接消息|
|`receiveMsg::voice`|语音消息|
|`receiveEvent::subscribe`|关注事件|
|`receiveEvent::unsubscribe`|取消关注事件|
|`receiveEvent::scan`|扫描带参数二维码事件|
|`receiveEvent::location`|上报地理位置事件|
|`receiveEvent::click`|自定义菜单事件|
|`receiveAllEnd`|全局结束|
|`accessCheckSuccess`|URL 有效性验证|
|`404`|签名验证错误|

### 注册参数包含以下内容

|KEY|含义|
|-----:|-----|
|`plugin`|插件标识|
|`include`|含有待注册方法的文件，此文件应位于您所注册的插件的目录下|
|`class`|待注册方法的类名|
|`method`|待注册方法名|

### 范例
```
$data = array(
	'receiveMsg::text' => array(
		'plugin' => 'wechat',
		'include' => 'response.class.php',
		'class' => 'WSQResponse',
		'method' => 'text'
	),
	'receiveEvent::subscribe' => array(
		'plugin' => 'wechat',
		'include' => 'response.class.php',
		'class' => 'WSQResponse',
		'method' => 'subscribe'
	),
);
WeChatHook::updateResponse($data);
```
> 接口响应的相关参数将提交给已注册方法的第一个参数中

## 跳转嵌入点
### 嵌入点参数包含以下内容

|KEY|含义|
|-----:|-----|
|`plugin`|插件标识|
|`include`|含有待注册方法的文件，此文件应位于您所注册的插件的目录下|
|`class`|待注册方法的类名|
|`method`|待注册方法名|


### 范例
```
$data = array(
	'plugin' => 'wechat',
	'include' => 'response.class.php',
	'class' => 'WSQResponse',
	'method' => 'redirect'
);
WeChatHook::updateRedirect($data);
```

## 服务端接口
服务端接口用于在嵌入点的方法中，格式化回复给微信用户的内容而设定的一系列函数。接口会自动转码（输入内容不会转码，请自行用 diconv 转码）

### 文本消息
```
WeChatServer::getXml4Txt($txt)
```

|参数|参数含义|
|-----:|-----|
|`$txt`|文本内容|


### 图片消息
```
WeChatServer::getXml4ImgByMid($mid)
```

|参数|参数含义|
|-----:|-----|
|`$mid`|媒体 ID，通过“WeChatClient->upload”方法上传多媒体文件后得到的 ID|

### 语音消息
```
WeChatServer::getXml4VoiceByMid($mid)
```

|参数|参数含义|
|-----:|-----|
|`$mid`|媒体 ID，通过“WeChatClient->upload”方法上传多媒体文件后得到的 ID|

### 视频消息
```
WeChatServer::getXml4VideoByMid($mid, $title, $desc = '')
```

|参数|参数含义|
|-----:|-----|
|`$mid`|媒体 ID，通过“WeChatClient->upload”方法上传多媒体文件后得到的 ID|
|`$title`|视频消息的标题|
|`$desc`|视频消息的描述|

### 音乐消息
```
WeChatServer::getXml4MusicByUrl($url, $thumbmid, $title, $desc = '', $hqurl = '')
```

|参数|参数含义|
|-----:|-----|
|`$url`|音乐链接|
|`$thumbmid`|缩略图 ID，通过“$WeChatClient->upload”方法上传多媒体文件后得到的 ID|
|`$title`|音乐标题|
|`$desc`|音乐描述|
|`$hqurl`|高质量音乐链接|

### 图文消息
```
WeChatServer::getXml4RichMsgByArray($list)
```
> 最多 10 条

|参数|参数含义|
|-----:|-----|
|`$list['title']`|图文消息标题|
|`$list['desc']`|图文消息描述|
|`$list['pic']`|图片链接|
|`$list['url']`|跳转链接|

## 客户端接口
客户端接口提供了一些微信公众平台的众多高级功能，这些接口方法需要微信公众平台的 AppId 和 AppSecret

### 上传下载多媒体文件
```
$WeChatClient->upload($type, $file_path, $mediaidOnly = 1)
```
```
$WeChatClient->download($mid)
```

### 自定义菜单
`$WeChatClient->getMenu()`

`$WeChatClient->deleteMenu()`

`$WeChatClient->setMenu($myMenu)`

### 发送客服消息
`$WeChatClient->sendTextMsg($to, $msg)`

`$WeChatClient->sendImgMsg($to, $mid)`

`$WeChatClient->sendVoice($to, $mid)`

`$WeChatClient->sendVideo($to, $mid, $title, $desc)`

`$WeChatClient->sendMusic($to, $url, $thumb_mid, $title, $desc = '', $hq_url = '')`

`$WeChatClient->sendRichMsg($to, $articles)`


### 用户管理
`$WeChatClient->createGroup($name)`

`$WeChatClient->renameGroup($gid, $name)`

`$WeChatClient->moveUserById($uid, $gid)`

`$WeChatClient->getAllGroups()`

`$WeChatClient->getGroupidByUserid($uid)`

`$WeChatClient->getUserInfoById($uid, $lang = '')`

`$WeChatClient->getFollowersList($next_id = '')`

`$WeChatClient->getOAuthConnectUri($redirect_uri, $state = '', $scope = 'snsapi_base')`

`$WeChatClient->getAccessTokenByCode($code)`

`$WeChatClient->refreshAccessTocken($refresh_token)`

`$WeChatClient->getUserInfoByAuth($access_token, $openid, $lang = 'zh_CN')`

### 二维码
```
$WeChatClient->getQrcodeImgByTicket($ticket)
```
```
$WeChatClient->getQrcodeImgUrlByTicket($ticket)
```
```
$WeChatClient->getQrcodeTicket($options = array())
```

<p class="xg1 y">更新时间：2014-4-28</p>