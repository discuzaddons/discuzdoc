common_credit_log 表中的 operation 的取值对应的含义定义如下：

|简写|关联id|含义|
|----|----|----|
|TRC|common_task.taskid|任务奖励积分|
|RTC|forum_thread.tid|发表悬赏主题扣除积分|
|RAC|forum_thread.tid|最佳答案获取悬赏积分|
|MRC|common_magic.mid|道具随即获取积分 magic rand credit|
|BGC|common_magic.mid|埋下红包|
|RGC|common_magic.mid|回收红包|
|AGC|common_magic.mid|获得红包|
|BMC|common_magic.mid|购买道具|
|TFR|common_member.uid|积分转账转出|
|RCV|common_member.uid|积分转账接收|
|CEC|common_member.uid|积分兑换 credit exchange|
|ECU|common_member.uid|通过ucenter 兑换积分 exchange credit ucenter|
|SAC|forum_attachment.aid|出售附件获得积分 sale attachment credit|
|BAC|forum_attachment.aid|购买附件支出积分 buy attachment credit|
|PRC|forum_post.pid|帖子被评分所得积分  post rate credit|
|RSC|forum_post.pid|评分帖子扣除自己的积分  post rate credit|
|STC|forum_thread.tid|出售主题获得积分 sale thread credit|
|BTC|forum_thread.tid|购买主题支出积分 buy thread credit|
|AFD|common_member.uid|购买积分即积分充值|
|UGP|common_usergroup.groupid|购买扩展用户组支出积分|
|RPC|common_report.id|举报功能中的奖惩|
|ACC|forum_activity.tid|参与活动扣除积分|
|RCT|forum_thread.tid|回帖奖励积分的扣除自己积分 reply credits thread|
|RCA|forum_posts.tid|回帖奖励积分用户获得积分的记录 reply credits attain|
|RCB|forum_thread.tid|回帖奖励积分用户编辑奖励主题返还积分的记录 reply credits back|
|CDC|无|充值卡密操作积分记录， CarD Credit|
|RKC|home_show.uid|排行榜竞价排名 RanKlist Credit|
|BME|forum_medal.medalid|购买勋章|
|AUT|forum_thread.tid|为主题参与者打标签|