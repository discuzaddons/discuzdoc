## 创建模板
\template\default\common\目录下创建addon_news.htm文件，代码如下：
```
<!--{template common/header}-->
<div id="pt" class="bm cl">
        <div class="z">
                <a href="./" class="nvhm" title="{lang homepage}">$_G[setting][bbname]</a> <em>›</em>
                <a href="news.php">热点新闻</a>
        </div>
</div>
 
<!--{ad/text/wp a_t}-->
<style id="diy_style" type="text/css"></style>
<div class="wp">
        <!--[diy=diy1]--><div id="diy1" class="area"></div><!--[/diy]-->
</div>
<div id="ct" class="ct2 wp cl">
        <!--在这里面写入页面中间内容-->
</div>
<!--{template common/footer}-->
```

## 创建入口文件
在论坛根目录下创建news.php文件，代码如下：
```
<?php
require_once './source/class/class_core.php';
$discuz = C::app();
$discuz->init();
//这里是代码逻辑
include template('diy:common/addon_news');
```

## 前台访问地址
> http://域名/news.php

完成以上部分，一个单页框架就搭建起来了，剩下的就是完善自己的业务逻辑了！